package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieListMapper implements Converter<List<Movie>, List<MovieDto>> {

    private Converter<Movie, MovieDto> convertToDto = (Movie movie) -> {
        return new MovieDto(movie.getMovieId(), movie.getTitle(), movie.getImage(), movie.getYear());
    };

    @Override
    public List<MovieDto> convert(List<Movie> from) {
        return from.stream()
                .map(movie -> convertToDto.convert(movie))
                .collect(Collectors.toList());    }
}
