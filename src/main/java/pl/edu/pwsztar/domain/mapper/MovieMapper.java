package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<CreateMovieDto, Movie> {

    private Converter<CreateMovieDto, Movie> convertToEntity = (CreateMovieDto movieDto) -> {
        return new Movie(movieDto.getTitle(), movieDto.getImage(), movieDto.getYear());
    };

    @Override
    public Movie convert(CreateMovieDto from) {
        return convertToEntity.convert(from);
    }
}
